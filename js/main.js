(function() {
  var canvas = document.querySelector('#canvas'); 
  var ctx = canvas.getContext("2d");
  var stageArray = [];
  var speeds = [1,2,3,4,5,6,7,8,9,10];
  var lastRender;
  var directions = ['oost','west','noord','zuid']
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;

  class ImageWrapper {
		img;
		width=0;
		height=0;
		scale = 1; 
		scaledWidth=0;
		scaledHeight=0;
		x=0;
		y=0;
		speed = 1;

    constructor(src,direction,speed,ctx){
			this.direction = direction;
			this.ctx = ctx;
			this.img = new Image();
			var img = this.img;
      this.speed = speed;

			var that = this;
			this.img.onload = function() {
				var scaleH=1,scaleV=1;
				that.width = img.width;
				that.height = img.height;				
				if (img.width > canvas.width){
					scaleH = canvas.width/img.width;
				}				
				if (img.height > canvas.height){
					scaleV = canvas.height/img.height;
				}
				if (scaleV||scaleH){
					that.scale = Math.min(scaleV,scaleH);
				}
				that.scaledWidth = that.scale*that.width;
				that.scaledHeight = that.scale*that.height; 

				if (that.direction == 'oost'){
					that.x = canvas.width;
					that.y = canvas.height/2 - that.scaledHeight/2;
				} else if(that.direction == 'west'){
					that.x = -1*that.scaledWidth;
					that.y = canvas.height/2 - that.scaledHeight/2;
				} else if (that.direction == 'noord'){
					that.y = canvas.height;
					that.x = canvas.width/2 - that.scaledWidth/2;
				} else if(that.direction == 'zuid'){
					that.y = -1* that.scaledHeight;
					that.x = canvas.width/2 - that.scaledWidth/2;
				}
			};
			this.img.src = src; 
		}

		update(progress){
			let addition = progress/this.speed;
			if (this.direction == 'oost'){
				this.x -= addition;
			} else if(this.direction == 'west'){
				this.x += addition;
			} else if (this.direction == 'noord'){
				this.y -= addition;
			} else if(this.direction == 'zuid'){
				this.y += addition;
			}
		}

		isgone(){
			if (!this.width) return false; // not fully loaded yet

			if (this.direction == 'oost' && this.x < -1*this.scaledWidth){
				return true;
			} else if(this.direction == 'west' && this.x > canvas.width){
				return true;
			} else if (this.direction == 'noord' && this.y < -1*this.scaledHeight){
				return true;
			} else if (this.direction == 'zuid' && this.y > canvas.height){
				return true;
			}
			return false; 
		}

		draw(){
			if (this.width > 0){
   			ctx.drawImage(this.img, this.x, this.y,this.scaledWidth,this.scaledHeight);
			}
		}
	}


	function update(progress){
		for (var [index,i] of stageArray.entries()){
			if (typeof i != 'undefined'){
				i.update(progress)
				if (i.isgone()){
					//delete (stageArray[index])
					stageArray.length=0;
					getImage();
				}
			}
		} 
	}


	function draw(){
		ctx.clearRect(0, 0, canvas.width, canvas.height)
		for (var i of stageArray){
			if (typeof i != 'undefined')	i.draw()
		} 
	}


	function loop(timestamp) {
		var progress = timestamp - lastRender
		update(progress)
		draw()
		lastRender = timestamp
		window.requestAnimationFrame(loop)
	}


	function getImage(){
		var r = new XMLHttpRequest();
		r.onreadystatechange = function(){
			if (r.readyState === XMLHttpRequest.DONE) {
				if (r.status === 200) {
					var d = directions[Math.floor(Math.random() * directions.length)];	
          var s = speeds[Math.floor(Math.random() * speeds.length)];
					stageArray.push(new ImageWrapper(r.responseText,d,s,ctx));
				} else {
					alert(':-(');
				}
			} 
		};
		r.open('GET', './getpicture.php', true);
		r.send();
	}

 	getImage(); 
	window.requestAnimationFrame(loop)
})(); 
